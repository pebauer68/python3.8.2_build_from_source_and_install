#!/bin/bash
#get python 3.8.2 running on Ubuntu 18.04 
#check if dir ~/src_python is there
#python 3.8.2 will be installed in dir:
#/usr/local/bin
#Ubuntu itself keeps installation in dir:
#/usr/bin
if [ -d "/home/$USER/src_python" ] 
  then 
    echo "Directory ~/src_python already exists."
  else
    echo "Directory ~/src_python is created." 
    mkdir -p /home/$USER/src_python
  fi
#download python and build/make/make install
    mkdir -p /home/$USER/src_python/python_3.8.2
    cd /home/$USER/src_python/python_3.8.2
    wget https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tar.xz
      if [ -f Python-3.8.2.tar.xz ]
        then  
          echo "Python Download ok"
          xz -d Python-3.8.2.tar.xz
          tar -xvf Python-3.8.2.tar
          cd Python-3.8.2
          ./configure && make && make test
          sudo make install
      fi


